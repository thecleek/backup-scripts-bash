#!/usr/bin/env bash
## vim:ts=4:sw=4:tw=200:nu:ai:nowrap:
##
## application library for bashinator example application
##
## Created by Wolfram Schlich <wschlich@gentoo.org>
## Licensed under the GNU GPLv3
## Web: http://www.bashinator.org/
## Code: https://github.com/wschlich/bashinator/
##

##
## REQUIRED PROGRAMS
## =================
## - rsync
##

##
## application initialization function
##

function __init() {

	## -- BEGIN YOUR OWN APPLICATION INITIALIZATION CODE HERE --
	STARTTIME=$(date +%s)
	__msg info "Starting"

	if ! __requireCommand rsync; then
		__die 4 "Command missing: rsync"
	fi

	return 0 # success

	## -- END YOUR OWN APPLICATION INITIALIZATION CODE HERE --

}

##
## application main function
##

function __main() {

	## -- BEGIN YOUR OWN APPLICATION MAIN CODE HERE --
	# shellcheck source=backup-rsync.cfg.sh
	source "${ApplicationConfig}"

	if ! createDirectory "${BackupPath}"; then
		__die 2 "Failed to create ${BackupPath}"
	fi

	# local i
	# for i in debug info notice warning err crit alert emerg; do
	# 	__msg ${i} "this is a ${i} test"
	# done
	local hasErrors=0
	for origin in "${!BackupList[@]}"; do
		destinationSubfolder="${BackupList[${origin}]}"
		destination="${BackupPath}${destinationSubfolder}"
		__msg debug "${origin} -> ${destinationSubfolder} -> ${destination}"
		if ! createDirectory "${destination}" ; then
			__die 2 "Failed to create ${destination}"
		fi

		__msg debug "Starting rsync ${origin} -> ${destination}"
		if rsync --archive --progress --compress --protect-args --verbose "${origin}" "${destination}" >>"${_L}" 2>&1; then
			__msg notice "Successful rsync ${origin} -> ${destination}"
		else
			__msg err "Couldn't rsync ${origin} -> ${destination}"
			hasErrors=1
			if [[ ${DieOnError} == 1 ]]; then
				__die 6 "There were some errors rsyncing ${origin} -> ${destination}"
			fi
		fi
	done

	if [[ ${hasErrors} == 1 ]]; then
		__die 5 "There were some errors. Please check the log."
	fi

	ENDTIME=$(date +%s)
	__msg info "Done :) in $((ENDTIME - STARTTIME)) seconds"


	return 0 # success

	## -- END YOUR OWN APPLICATION MAIN CODE HERE --

}

function createDirectory() {
	local directory=${1}
	if [[ -z "${directory}" ]]; then
		__msg err "argument 1 (directory) missing"
		return 2 # error
	fi
	if [[ ! -d "${directory}" ]]; then
		__msg debug "Creating directory: ${directory}"
		if ! mkdir -p "${directory}" >>"${_L}" 2>&1; then
			__msg err "Failed to create directory '${directory}'"
			return 2 # error
		fi
		__msg notice "Created ${directory}"
	fi
	return 0 # success
}
