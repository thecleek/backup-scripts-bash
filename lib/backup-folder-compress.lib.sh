#!/usr/bin/env bash
## vim:ts=4:sw=4:tw=200:nu:ai:nowrap:
##
## application library for bashinator example application
##
## Created by Wolfram Schlich <wschlich@gentoo.org>
## Licensed under the GNU GPLv3
## Web: http://www.bashinator.org/
## Code: https://github.com/wschlich/bashinator/
##

##
## REQUIRED PROGRAMS
## =================
## - tar
##

##
## application initialization function
##

function __init() {

	## -- BEGIN YOUR OWN APPLICATION INITIALIZATION CODE HERE --
	STARTTIME=$(date +%s)
	__msg info "Starting"

	if ! __requireCommand tar; then
		__die 4 "Command missing: rsync"
	fi

	return 0 # success

	## -- END YOUR OWN APPLICATION INITIALIZATION CODE HERE --

}

##
## application main function
##

function __main() {

	# shellcheck source=backup-folder-compress.cfg.sh
	source "${ApplicationConfig}"

	if ! createDirectory "${BackupPath}"; then
		__die 2 "Failed to create ${BackupPath}"
	fi

	# local i
	# for i in debug info notice warning err crit alert emerg; do
	# 	__msg ${i} "this is a ${i} test"
	# done

	local fromdir=""
	local tmp
	local hasErrors=0

	for origin in "${!BackupList[@]}"; do
		destinationSubfolder="${BackupList[${origin}]}"
		destination="${BackupPath}${destinationSubfolder}"
		__msg debug "${origin} -> ${destinationSubfolder} -> ${destination}"
		if ! createDirectory "${destination}" ; then
			__die 2 "Failed to create ${destination}"
		fi

		fromdir=$(dirname "${origin}")
		bdir=$(basename "${origin}")
		# __msg debug "${fromdir}"

		# Because tar is hell trying to remove absolute paths
		# https://stackoverflow.com/questions/18681595/tar-a-directory-but-dont-store-full-absolute-paths-in-the-archive
		pushd "${fromdir}" &>>"${_L}"
		__msg debug "Changing temporarly to directory $(pwd)"

		Filename="${FilenamePrefix}${destinationSubfolder}${FilenameSuffix}"

		__msg debug "Compressing ${destinationSubfolder} -> ${Filename}"
		if ! tar \
			--verbose \
			--create \
			--bzip2 \
			--file="${destination}/${Filename}" \
				"${bdir}" &>>"${_L}" ; then
			hasErrors=1
			__msg crit "Could not compress folder ${Filename}"
			if [[ ${StopOnDumpError} == 1 ]] ; then
				__die 6 "Error compressing  ${Filename}"
			fi
		else
			__msg notice "Correctly created ${destination}/${Filename}"
		fi

		if ! md5sum "${destination}/${Filename}" > "${destination}/${Filename}.md5" ; then
			__msg warning "Could not create MD5 hash"
		else
			__msg debug "Created MD5 hash -> $(cat "${destination}/${Filename}.md5")"
		fi

		popd  &>>"${_L}"
		__msg debug "Returning to directory $(pwd)"
	done

	if [[ ${hasErrors} == 1 ]]; then
		__die 5 "There were some errors. Please check the log."
	fi

	ENDTIME=$(date +%s)
	__msg info "Done :) in $((ENDTIME - STARTTIME)) seconds"


	return 0 # success

	## -- END YOUR OWN APPLICATION MAIN CODE HERE --

}

function createDirectory() {
	local directory=${1}
	if [[ -z "${directory}" ]]; then
		__msg err "argument 1 (directory) missing"
		return 2 # error
	fi
	if [[ ! -d "${directory}" ]]; then
		__msg debug "Creating directory: ${directory}"
		if ! mkdir -p "${directory}" >>"${_L}" 2>&1; then
			__msg err "Failed to create directory '${directory}'"
			return 2 # error
		fi
		__msg notice "Created ${directory}"
	fi
	return 0 # success
}
