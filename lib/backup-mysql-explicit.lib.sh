#!/usr/bin/env bash
## vim:ts=4:sw=4:tw=200:nu:ai:nowrap:
##
## application library for bashinator example application
##
## Created by Wolfram Schlich <wschlich@gentoo.org>
## Licensed under the GNU GPLv3
## Web: http://www.bashinator.org/
## Code: https://github.com/wschlich/bashinator/
##

##
## REQUIRED PROGRAMS
## =================
## - rm
## - mkdir
## - ls
##

##
## application initialization function
##

function __init() {

	## -- BEGIN YOUR OWN APPLICATION INITIALIZATION CODE HERE --
	STARTTIME=$(date +%s)
	__msg info "Starting"
	if ! __requireCommand mysql; then
		__die 4 "Command missing: mysql "
	fi
	if ! __requireCommand mysqldump; then
		__die 4 "Command missing: mysql "
	fi
	# if ! __requireCommand tail; then
	# 	__die 4 "Command missing: tail "
	# fi
	# if ! __requireCommand sort; then
	# 	__die 4 "Command missing: sort "
	# fi

	DBS_TO_BACKUP=${DBS_TO_BACKUP-()}
	StopOnDumpError=${StopOnDumpError-0}
	CompressFiles=${CompressFiles-0}
	CompressRemoveIntermediateFiles=${CompressRemoveIntermediateFiles:-1}

	return 0 # success

	## -- END YOUR OWN APPLICATION INITIALIZATION CODE HERE --

}

##
## application main function
##

function __main() {

	## -- BEGIN YOUR OWN APPLICATION MAIN CODE HERE --
	# shellcheck source=backup-rsync.cfg.sh
	source "${ApplicationConfig}"

	if ! createDirectory "${BackupPath}"; then
		__die 2 "Failed to create ${BackupPath}"
	fi

	if ! testConnection; then
		__die 3 "Error testing connection"
	fi

	if ! dbs_str=$(getDatabases); then
		__die 5 "Error obtaining databases"
	fi

	declare -a filesCreated
	declare -a filesToSetPermissions

	for namebase in "${DBS_TO_BACKUP[@]}"; do
		if [[ " ${DBS_OMIT_ALWAYS[@]} " =~ " ${namebase} " ]]; then
			__msg info "Omitting-always database ${namebase}"
			# echo "Omitiendo ${namebase}"
			continue
		fi

		filesCreated=()

		Filename_noDir="${FilenamePrefix}${namebase}${FilenameSuffix}"
		Filename="${BackupPath}${Filename_noDir}"

		__msg info "Retrieving database ${namebase} -> ${Filename}"

		if ! mysqldump \
			--user="${MYSQL_USER}" \
			--password="${MYSQL_PASS}" \
			--add-drop-database \
			--add-drop-table \
			--dump-date \
			--events \
			--routines \
			--triggers \
			--extended-insert \
			--complete-insert \
			--net_buffer_length=5120 \
			--add-locks \
			--opt \
			--quick \
			--result-file="${Filename}" \
			--verbose \
				"${namebase}" 2>>"${_L}" ; then
			__msg crit "Could not backup database ${namebase}"
			if [[ ${StopOnDumpError} == 1 ]] ; then
				__die 6 "Error backing up ${namebase}"
			fi
		else
			__msg notice "Database ${namebase} is in a SQL dump file"
			filesCreated+=("${Filename_noDir}")
		fi

		hasMD5=0
		if ! md5sum "${Filename}" > "${Filename}.md5" ; then
			__msg warning "Could not create MD5 hash"
		else
			__msg debug "Created MD5 hash for ${Filename} -> $(cat "${Filename}.md5")"
			filesCreated+=("${Filename_noDir}.md5")
			hasMD5=1
		fi

		if [[ ${CompressFiles} == 1 ]] ; then
			if ! tar \
					--create \
					--sparse \
					--bzip2 \
					--file="${Filename}.tar.bz2" \
					--directory="${BackupPath}" \
						"${filesCreated[@]}" ; then
				__msg crit "Could not compress database ${namebase}"
				if [[ ${StopOnDumpError} == 1 ]] ; then
					__die 6 "Error backing up ${namebase}"
				fi
			else
				filesToSetPermissions+=("${Filename_noDir}.tar.bz2")
				if [[ ${CompressRemoveIntermediateFiles} == 1 ]]; then
					for file in "${filesCreated[@]}"; do
						if rm -f "${BackupPath}${file}"; then
							__msg debug "Removed ${file}"
						else
							filesToSetPermissions+=("${file}")
							__msg debug "Failed to remove ${file}"
						fi
					done
				fi
				__msg notice "Created compressed file ${Filename}.tar.bz2"
			fi
		else
			filesToSetPermissions=("${filesCreated[@]}")
		fi

		for file in "${filesToSetPermissions[@]}"; do
			__msg info "Setting group and permissions to '${BackupPath}${file}'"
			if chmod "${FILES_MODE}" "${BackupPath}${file}"; then
				__msg debug "Changed permissions of ${file} to ${FILES_MODE}"
			else
				__msg warning "Failed to change permissions of ${file} to ${FILES_MODE}"
			fi
			if chgrp "${FILES_GROUP}" "${BackupPath}${file}"; then
				__msg debug "Changed group of '${file}' to ${FILES_GROUP}"
			else
				__msg warning "Failed to change group of '${file}' to ${FILES_GROUP}"
			fi
		done


	done


	# local i
	# for i in debug info notice warning err crit alert emerg; do
	# 	__msg ${i} "this is a ${i} test"
	# done
	ENDTIME=$(date +%s)
	__msg info "Done :) in $((ENDTIME - STARTTIME)) seconds"


	return 0 # success

	## -- END YOUR OWN APPLICATION MAIN CODE HERE --

}

function testConnection()	{
	if ! mysql \
			--user="${MYSQL_USER}" \
			--password="${MYSQL_PASS}" \
			--execute "quit" \
				 2>>"${_L}"; then
		__msg emerg "Failed connecting to mysql"
		return 2
	fi
	__msg notice "MySQL succesfully connected"
	return 0
}

function getDatabases()	{
	mysql \
		--batch \
		--disable-column-names \
		--user="${MYSQL_USER}" \
		--password="${MYSQL_PASS}" \
		--execute "SHOW DATABASES" 2>"${_L}"
}

function createDirectory() {
	local directory=${1}
	if [[ -z "${directory}" ]]; then
		__msg err "Argument 1 (directory) missing"
		return 2 # error
	fi
	if [[ ! -d "${directory}" ]]; then
		__msg debug "Creating directory: ${directory}"
		if ! mkdir -p "${directory}" >>"${_L}" 2>&1; then
			__msg err "Failed to create directory '${directory}'"
			return 2 # error
		fi
		__msg notice "Created ${directory}"
	fi
	return 0 # success
}
