#!/usr/bin/env bash
## vim:ts=4:sw=4:tw=200:nu:ai:nowrap:
##
## application library for bashinator example application
##
## Created by Wolfram Schlich <wschlich@gentoo.org>
## Licensed under the GNU GPLv3
## Web: http://www.bashinator.org/
## Code: https://github.com/wschlich/bashinator/
##

##
## REQUIRED PROGRAMS
## =================
## - rm
## - mkdir
## - ls
##

##
## application initialization function
##

function __init() {

	## -- BEGIN YOUR OWN APPLICATION INITIALIZATION CODE HERE --
	STARTTIME=$(date +%s)
	__msg info "Starting"
	if ! __requireCommand mysql; then
		__die 4 "Command missing: mysql "
	fi
	if ! __requireCommand mysqldump; then
		__die 4 "Command missing: mysql "
	fi
	# if ! __requireCommand tail; then
	# 	__die 4 "Command missing: tail "
	# fi
	# if ! __requireCommand sort; then
	# 	__die 4 "Command missing: sort "
	# fi

	DBS_OMIT=${DBS_OMIT-()}
	DBS_ONLY_STRUCTURE=${DBS_ONLY_STRUCTURE-()}
	StopOnDumpError=${StopOnDumpError-0}
	CompressFiles=${CompressFiles-0}
	CompressRemoveIntermediateFiles=${CompressRemoveIntermediateFiles:-1}

	return 0 # success

	## -- END YOUR OWN APPLICATION INITIALIZATION CODE HERE --

}

##
## application main function
##

datadir=""

function __main() {

	## -- BEGIN YOUR OWN APPLICATION MAIN CODE HERE --
	# shellcheck source=backup-rsync.cfg.sh
	source "${ApplicationConfig}"
	local exitcode

	if ! findMySQLDatadir; then
		__die 9 "Could not find MySQL datadir"
	else
		__msg info "Found MySQL datadir in ${datadir}"
	fi

	if [[ ! -r "${datadir}" ]]; then
		__die 9 "Directory ${datadir} is not readable by user $(whoami)"
	fi

	current_timestamp=$(date +%s)
	initial_timestamp=${current_timestamp}
	local diff_timestamp
	while isMySQLRunning; do
		current_timestamp=$(date +%s)
		diff_timestamp=$(( current_timestamp - initial_timestamp ))
		# __msg info "initial_timestamp ${initial_timestamp}   current_timestamp ${current_timestamp}    diff_timestamp ${diff_timestamp}"
		if [[ diff_timestamp -ge 40 ]]; then
			__die 8 "MySQL did not stop"
		fi
		if ! stopMySQL; then
			__die 6 "Failed in trying to stop MySQL"
		fi
		__msg debug "Waiting 5 seconds for mysql to stop"
		sleep 5s
	done

	__msg notice "MySQL is stopped"

	if ! createDirectory "${BackupPath}"; then
		__die 2 "Failed to create ${BackupPath}"
	fi

	__msg info "Starting sync ${datadir} -> ${BackupPath}"

	if rsync --archive --progress --compress --protect-args --verbose "${datadir}" "${BackupPath}" >>"${_L}" 2>&1; then
		__msg notice "Synced"
	else
		__die 7 "Failed to sync"
	fi

	while ! isMySQLRunning; do
		current_timestamp=$(date +%s)
		diff_timestamp=$(( current_timestamp - initial_timestamp ))
		# __msg info "initial_timestamp ${initial_timestamp}   current_timestamp ${current_timestamp}    diff_timestamp ${diff_timestamp}"
		if [[ diff_timestamp -ge 40 ]]; then
			__die 8 "MySQL did not start"
		fi
		if startMySQL; then
			__msg notice "Success starting MySQL"
			break
		fi
		__msg debug "Waiting 5 seconds for mysql to start"
		sleep 5s
	done




	# local i
	# for i in debug info notice warning err crit alert emerg; do
	# 	__msg ${i} "this is a ${i} test"
	# done
	ENDTIME=$(date +%s)
	__msg info "Done :) in $((ENDTIME - STARTTIME)) seconds"


	return 0 # success

	## -- END YOUR OWN APPLICATION MAIN CODE HERE --

}

function startMySQL()	{
	__msg info "Trying to start MySQL"
	/usr/sbin/service mysql start
}

function stopMySQL()	{
	__msg info "Trying to stop MySQL"
	/usr/sbin/service mysql stop
}

function isMySQLRunning()	{
	local exitcode
	# __msg debug "Is MySQL running?"
	/etc/init.d/mysql status &>>"${_L}"
	exitcode=$?
	local yesno
	if [[ ${exitcode} -eq 0 ]]; then
		yesno="Yes"
	else
		yesno="No"
	fi
	__msg debug "Is MySQL running? ${yesno}"
	return ${exitcode}
}

function findMySQLDatadir()	{
	datadir=$(/usr/sbin/mysqld --verbose --help | grep "^datadir" 2>>"${_L}")  # Gets the datadir but with an overhead
	exitcode=$?
	datadir="${datadir:7}"  # Removes the overhead but leaves some spaces
	# datadir=$(echo "${datadir}" | xargs)  # This will trim the string
	datadir="${datadir##*( )}"  # Removes spaces from the beginning
	datadir="${datadir%%*( )}"  # Removes spaces from the end
	return ${exitcode}
}

function testConnection()	{
	if ! mysql \
			--user="${MYSQL_USER}" \
			--password="${MYSQL_PASS}" \
			--execute "quit" \
				 2>>"${_L}"; then
		__msg emerg "Failed connecting to mysql"
		return 2
	fi
	__msg notice "MySQL succesfully connected"
	return 0
}

function getDatabases()	{
	mysql \
		--batch \
		--disable-column-names \
		--user="${MYSQL_USER}" \
		--password="${MYSQL_PASS}" \
		--execute "SHOW DATABASES" 2>"${_L}"
}

function createDirectory() {
	local directory=${1}
	if [[ -z "${directory}" ]]; then
		__msg err "Argument 1 (directory) missing"
		return 2 # error
	fi
	if [[ ! -d "${directory}" ]]; then
		__msg debug "Creating directory: ${directory}"
		if ! mkdir -p "${directory}" >>"${_L}" 2>&1; then
			__msg err "Failed to create directory '${directory}'"
			return 2 # error
		fi
		__msg notice "Created ${directory}"
	fi
	return 0 # success
}
