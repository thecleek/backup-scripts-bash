#!/usr/bin/env bash
## vim:ts=4:sw=4:tw=200:nu:ai:nowrap:
##
## Installation
##
## Author: Miguel Reséndiz <miguel@yektia.com>
## Licensed under the GNU GPLv3
## Web: http://www.nocturno.com.mx/
## Code: https://github.com/wschlich/bashinator/
##

##
## NOTES
## =====
## - you have to run 'bash -O extglob -O extdebug -n thisscriptfile' to test this script!
## - if you want to test this script right away, use the following command:
##   $ env __BashinatorConfig=bashinator.cfg.sh __BashinatorLibrary=../bashinator.lib.0.sh ApplicationConfig=example.cfg.sh ApplicationLibrary=example.lib.sh ./example.sh -a
##

##
## bashinator basic variables
##

export __ScriptFile=${0##*/} # evaluates to "backupinstall.sh"
export __ScriptName=${__ScriptFile%.sh} # evaluates to "backupinstall"

# export __ScriptPath=${0%/*}; __ScriptPath=${__ScriptPath%/} # evaluates to /path/to/example/example.sh
export __ScriptPath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"  # This resolves the path

__ScriptHost=$(hostname -f) # evaluates to the current hostname, e.g. host.example.com
export __ScriptHost
##
## bashinator library and config
##

export __BashinatorConfig="${__ScriptPath}/etc/bashinator.cfg.sh"
export __BashinatorLibrary="${__ScriptPath}/lib/bashinator/bashinator.lib.0.sh" # bashinator API v0

## include required source files
# shellcheck source=etc/bashinator.cfg.sh
if ! source "${__BashinatorConfig}"; then
    echo "!!! FATAL: failed to source bashinator config '${__BashinatorConfig}'" 1>&2
    exit 2
fi
# shellcheck source=lib/bashinator/bashinator.lib.0.sh
if ! source "${__BashinatorLibrary}"; then
    echo "!!! FATAL: failed to source bashinator library '${__BashinatorLibrary}'" 1>&2
    exit 2
fi

__boot

##
## application library and config
##

export ApplicationConfig="${__ScriptPath}/etc/${__ScriptName}.cfg.sh"
export ApplicationLibrary="${__ScriptPath}/lib/${__ScriptName}.lib.sh"

## include required source files (using bashinator functions with builtin error handling)
# shellcheck source=backup-folder-compress.cfg.sh
__requireSource "${ApplicationConfig}"

# shellcheck source=etc/backup-folder-compress.cfg.sh
source "${ApplicationConfig}"

# echo "${ApplicationLibrary}"
# shellcheck source=lib/backup-folder-compress.lib.sh
__requireSource "${ApplicationLibrary}"

##
## dispatch the application with all original command line arguments
##

__dispatch "${@}"
