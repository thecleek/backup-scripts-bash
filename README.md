# Backup scripts
These scripts are simple automated tasks:

* Backup all MySQL databases in an instance
* Backup database schemas and include specific tables
* rsync a list of folders

All the files are bash 4 compatible. It uses the [bashinator](https://github.com/wschlich/bashinator) framework.

[TOC]

## Features
* You write a destination folder, a list of the things you want and off you go.
* Gives you a log of the actions


## First clone the repo
```bash
git clone git@bitbucket.org:thecleek/backup-scripts-bash.git
cd backup-scripts-bash
# ... Edit the file
```
This gets you in the cloned folder. From there you have to config the scripts.


## Quick start
In the `etc` folder are samples of the configs. Just remove the `-SAMPLE` from the file names

Edit the files at your convenience and run the associated `.sh` file.

All have the same initial approach:
* Create a config file
* Modify it to your needs
* Run the scripts





## backup-rsync

1. Rename `etc/backup-rsync.cfg.sh-SAMPLE` -> `etc/backup-rsync.cfg.sh`
2. Edit the file `etc/backup-rsync.cfg.sh` to meet your needs
3. Run `./backup-rsync.sh`

```bash
cp etc/backup-rsync.cfg.sh-SAMPLE etc/backup-rsync.cfg.sh
nano etc/backup-rsync.cfg.sh
# ... Edit the file
```



### Config file

* BackupPath: Destination of the files
* BackupList: List of folders source and destination. You can add as many as you need.
* DieOnError: Flag to tell the script to die on the first error or keep going on and group all the errors (if any) at the end.

```bash
## vim:ts=4:sw=4:tw=200:nu:ai:nowrap:
##
## application config for backup-rsync
##

##
## application settings
##

BackupPath="/home/cleek/bkp/$(hostname)/"

# You can use variables
Today=$(date +"%F-%H-%M")

# origin -> destination
declare -x -A BackupList
BackupList["/home/cleek/Documents"]="Documents"
BackupList["user@REMOTE_HOST:/data/sql"]="sql"
BackupList["server:/data/sql"]="sql-${Today}"

# 1 -> Dies on first error
# 0 -> Continues even with errors and saves them to the end
# DieOnError default 0
DieOnError=1

```

You can also add ssh remote hosts
```bash
BackupList["user@REMOTE_HOST:/data/sql"]="sql"
```
And if you have them in your `~.ssh/config` file
```bash
BackupList["server:/data/sql"]="sql"
```
You can use regular bash commands and variables to append in the name
```bash
# You can use variables
Today=$(date +"%F-%H-%M")


BackupList["server:/data/sql"]="sql-${Today}"
```

### Run

```bash
./backup-rsync.sh
```

### Results
First, it gives you this structure:
```
/home/cleek/bkp/ada/
|- Documents/
|  - All the files in the source folder
|- sql/
|  - All the files in the source folder
|- sql-2020-08-18/
|  - All the files in the source folder
```
The output is in 2 places:
* first the stdout that should give you something like this
```
[2020-08-18 01:58:09.113] > [_____INFO] Starting
[2020-08-18 01:58:09.621] > [___NOTICE] Successful rsync /home/cleek/Documents -> /home/cleek/bkp/ada/Documents
[2020-08-18 01:58:09.636] > [_____INFO] Done :) in 0 seconds
```
* the second is the log in `logs/backup-rsync.log` that gives you more detailed info
```
backup-rsync[128691] [2020-08-18 01:58:09.113] > [_____INFO] {backup-rsync.lib.sh:26}, __init(): Starting
backup-rsync[128691] [2020-08-18 01:58:09.120] > [____DEBUG] {bashinator.lib.0.sh:1517}, __requireCommand(): command: rsync
backup-rsync[128691] [2020-08-18 01:58:09.131] > [____DEBUG] {bashinator.lib.0.sh:330}, __prepare(): successfully created temporary script subcommand logfile './logs/backup-rsync.log.azdu64'
backup-rsync[128691] [2020-08-18 01:58:09.138] > [____DEBUG] {bashinator.lib.0.sh:338}, __prepare(): script subcommand logfile: './logs/backup-rsync.log.azdu64'
backup-rsync[128691] [2020-08-18 01:58:09.154] > [____DEBUG] {backup-rsync.lib.sh:60}, __main(): /home/cleek/Documents -> emmy -> /home/cleek/bkp/ada/Documents
backup-rsync[128691] [2020-08-18 01:58:09.621] > [___NOTICE] {backup-rsync.lib.sh:66}, __main(): Successful rsync /home/cleek/Documents -> /home/cleek/bkp/ada/Documents
backup-rsync[128691] [2020-08-18 01:58:09.636] > [_____INFO] {backup-rsync.lib.sh:81}, __main(): Done :) in 0 seconds
backup-rsync[128691] [2020-08-18 01:58:09.644] > [____DEBUG] {bashinator.lib.0.sh:380}, __cleanup(): removing script subcommand logfile './logs/backup-rsync.log.azdu64'
backup-rsync[128691] [2020-08-18 01:58:09.656] > [____DEBUG] {bashinator.lib.0.sh:385}, __cleanup(): successfully removed script subcommand logfile './logs/backup-rsync.log.azdu64'
backup-rsync[128691] [2020-08-18 01:58:09.663] > [____DEBUG] {bashinator.lib.0.sh:1349}, __trapExit(): successfully mailed saved messages
```




## backup-mysql.sh

1. Rename `etc/backup-mysql.cfg.sh-SAMPLE` -> `etc/backup-mysql.cfg.sh`
2. Edit the file `etc/backup-mysql.cfg.sh` to meet your needs
3. Run `./backup-mysql.sh`

```bash
cp etc/backup-mysql.cfg.sh-SAMPLE etc/backup-mysql.cfg.sh
nano etc/backup-mysql.cfg.sh
# ... Edit the file
```



### Config file

* * BackupPath: Destination of the files
* MYSQL_HOST: MySQL Host to connect
* MYSQL_USER: MySQL User
* MYSQL_PASS: MySQL Password
* FILES_GROUP: Linux group for the resulting files
* FILES_MODE: Linux file mode
* FilenamePrefix: The filenames are ${FilenamePrefix}DATABASE${FilenameSuffix}
* FilenameSuffix: The filenames are ${FilenamePrefix}DATABASE${FilenameSuffix}
* DBS_OMIT_ALWAYS: Array of databases to omit always
* DBS_OMIT: Array of databases to omit from backup
* DBS_ONLY_STRUCTURE: Array of databases to extract only structure
* StopOnDumpError: Flag to stop on first error or saving them at the end
* CompressFiles: Flag. Should compress the files
* CompressRemoveIntermediateFiles: Flag. We always generate sql and md5 files. If CompressFiles, should these files be removed.


```bash
## vim:ts=4:sw=4:tw=200:nu:ai:nowrap:
##
## application config for backup-mysql
##

##
## application settings
##

MYSQL_HOST="localhost"
MYSQL_USER="cleek"
MYSQL_PASS='123456aA'

BackupPath="/home/cleek/bkp/$(hostname)/mysql-$(date +%F)/"

FILES_GROUP="cleek"
FILES_MODE=0600

FilenamePrefix="$(hostname)--$(date +%F--%H-%M)--"
FilenameSuffix=".sql"

# These DBs cannot be dumped
DBS_OMIT_ALWAYS=(information_schema performance_schema)

# If some databases need to be omited
# DBS_OMIT=(phpmyadmin sys)

# If for some databases, only the structure is needed
# DBS_ONLY_STRUCTURE=(projects)

# StopOnDumpError default 0
StopOnDumpError=1

# CompressFiles default 0
CompressFiles=1

# CompressRemoveIntermediateFiles default 1
CompressRemoveIntermediateFiles=1
```



### Run

```bash
./backup-mysql.sh
```

### Results
First, it gives you this structure:
```
/home/cleek/bkp/ada/mysql/2020-08-18/
|- All the created files
```
The output is in 2 places:
* first the stdout that should give you something like this
```
[2020-08-18 02:27:30.629] > [_____INFO] Starting
[2020-08-18 02:27:30.719] > [___NOTICE] Created /home/cleek/bkp/ada/mysql-2020-08-18/
[2020-08-18 02:27:30.762] > [___NOTICE] MySQL succesfully connected
[2020-08-18 02:27:31.097] > [_____INFO] Retrieving database churubusco -> /home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--churubusco.sql
[2020-08-18 02:27:31.212] > [___NOTICE] Database churubusco is in a SQL dump file
[2020-08-18 02:27:31.282] > [___NOTICE] Created compressed file /home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--churubusco.sql.tar.bz2
[2020-08-18 02:27:31.289] > [_____INFO] Setting group and permissions to '/home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--bivir.sql.tar.bz2'
[2020-08-18 02:27:31.319] > [_____INFO] Setting group and permissions to '/home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--churubusco.sql.tar.bz2'
[2020-08-18 02:27:31.695] > [_____INFO] Omitting-always database information_schema
[2020-08-18 02:27:31.697] > [_____INFO] Retrieving database mysql -> /home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--mysql.sql
[2020-08-18 02:27:31.943] > [___NOTICE] Database mysql is in a SQL dump file
[2020-08-18 02:27:32.076] > [___NOTICE] Created compressed file /home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--mysql.sql.tar.bz2
[2020-08-18 02:27:32.086] > [_____INFO] Setting group and permissions to '/home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--churubusco.sql.tar.bz2'
[2020-08-18 02:27:32.102] > [_____INFO] Setting group and permissions to '/home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--mysql.sql.tar.bz2'
[2020-08-18 02:27:32.110] > [_____INFO] Omitting-always database performance_schema
[2020-08-18 02:27:32.112] > [_____INFO] Retrieving database sys -> /home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--sys.sql
[2020-08-18 02:27:32.981] > [___NOTICE] Database sys is in a SQL dump file
[2020-08-18 02:27:33.100] > [___NOTICE] Created compressed file /home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--sys.sql.tar.bz2
[2020-08-18 02:27:33.103] > [_____INFO] Setting group and permissions to '/home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--bivir.sql.tar.bz2'
[2020-08-18 02:27:33.110] > [_____INFO] Setting group and permissions to '/home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--churubusco.sql.tar.bz2'
[2020-08-18 02:27:33.120] > [_____INFO] Setting group and permissions to '/home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--iebsa.sql.tar.bz2'
[2020-08-18 02:27:33.127] > [_____INFO] Setting group and permissions to '/home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--mysql.sql.tar.bz2'
[2020-08-18 02:27:33.135] > [_____INFO] Setting group and permissions to '/home/cleek/bkp/ada/mysql-2020-08-18/ada--2020-08-18--02-27--sys.sql.tar.bz2'
[2020-08-18 02:27:33.144] > [_____INFO] Done :) in 3 seconds
```
* the second is the log in `logs/backup-mysql.log` that gives you more detailed info. Wich I thought was too much to put is a readme.





## More configuration options
As this scripts use [bashinator](https://github.com/wschlich/bashinator), it has its own set of options that can be found in `etc/bashinator.cfg.sh`

These options cover emailing a report of the log, logging levels for the stdout, log and email. And a lot more, it's documented in the connfig file.

You can specify other config scripts using enviroment variables, like this:
```
env ApplicationConfig=backup-rsync-alternativeConfig.cfg.sh ./backup-rsync.sh
```

## License
This scripts are [GPL3](https://www.gnu.org/licenses/gpl-3.0.en.html)